# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "fishi0x01/win-10-pro-x64"

  config.vm.communicator = "winrm"
  
  # set some defaults
  config.vm.guest = :windows
  config.vm.hostname = "winhax"

  # forward RDP and WINRS ports
  config.vm.network :forwarded_port, guest: 3389, host: 3388, id: "rdp", auto_correct: false
  config.vm.network :forwarded_port, guest: 5985, host: 5985, id: "winrm", auto_correct: true
  config.windows.set_work_network = true
  config.winrm.max_tries = 10

  # install chocolatey
  config.vm.provision "shell", inline: "Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"

  # install tools via chocolatey
  config.vm.provision "shell", inline: "choco install 7zip -y"
  config.vm.provision "shell", inline: "choco install dnspyex -y"
  config.vm.provision "shell", inline: "choco install explorersuite -y"
  config.vm.provision "shell", inline: "choco install git -y"
  config.vm.provision "shell", inline: "choco install cutter -y"
  config.vm.provision "shell", inline: "choco install ghidra -y"
  config.vm.provision "shell", inline: "choco install x64dbg.portable -y"
  config.vm.provision "shell", inline: "choco install firefox -y"
  config.vm.provision "shell", inline: "choco install burp-suite-free-edition -y"
  config.vm.provision "shell", inline: "choco install fiddler -y"
  config.vm.provision "shell", inline: "choco install sysinternals -y"
  config.vm.provision "shell", inline: "choco install regshot -y"
  config.vm.provision "shell", inline: "choco install processhacker -y"
  config.vm.provision "shell", inline: "choco install linqpad -y"
  config.vm.provision "shell", inline: "choco install vmware-powercli-psmodule -y"
  config.vm.provision "shell", inline: "choco install az.powershell -y"
  config.vm.provision "shell", inline: "choco install python -y"
  config.vm.provision "shell", inline: "choco install nmap -y"
  config.vm.provision "shell", inline: "choco install wireshark -y"
  config.vm.provision "shell", inline: "choco install winpcap -y"
  config.vm.provision "shell", inline: "choco install javaruntime -y"
  config.vm.provision "shell", inline: "choco install javadecompiler-gui -y"
  config.vm.provision "shell", inline: "choco install ojdkbuild -y"
  config.vm.provision "shell", inline: "choco install microsoft-windows-terminal -y"
  config.vm.provision "shell", inline: "choco install vscode -y"
  config.vm.provision "shell", inline: "choco install visualstudio2019community -y"
    
  # git clone tools
  config.vm.provision "shell", inline: "New-Item -ItemType Directory -Force -Path $env:userprofile\\opt"
  config.vm.provision "shell", inline: "git clone https://github.com/pwntester/ysoserial.net $env:userprofile\\opt\\ysoserial.net"
  config.vm.provision "shell", inline: "git clone https://github.com/S3cur3Th1sSh1t/PowerSharpPack $env:userprofile\\opt\\PowerSharpPack"
  config.vm.provision "shell", inline: "git clone https://github.com/NetSPI/PESecurity $env:userprofile\\opt\\PESecurity"
  config.vm.provision "shell", inline: "git clone https://github.com/mattifestation/PowerShellArsenal $env:userprofile\\opt\\PowerShellArsenal"

  # Restart the machine having changed the hostname:
  config.vm.provision "shell", inline: "Restart-Computer -Force"
  
end
